# README #

A demonstration of using SSDT (Sql Server Data Tools) projects to maintain a MSSQL database.


### How do I get set up? ###

* Install SSDT : https://msdn.microsoft.com/en-us/library/mt204009.aspx
* Install MSBuild Extension Pack: http://www.msbuildextensionpack.com/
* The database publish profile assumes you have a default instance of MSSQL running on host "." or "localhost" (edit this as needed in  /eCommerce.SqlServer/PublishProfiles/Dev.publish.xml)
* In an MSBuild command prompt from the automation folder, run: msbuild BuildAndTest.csproj