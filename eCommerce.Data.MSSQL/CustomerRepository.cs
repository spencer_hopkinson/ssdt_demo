﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;

namespace eCommerce.Data.MSSQL
{
    public class Customer
    {
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
    }
    
    public class CustomerRepository
    {
        public Customer Get(int id)
        {
            using (var dbContext = new ECommerceDbContext())
            {
                return dbContext.Customers.SingleOrDefault(c => c.Id == id);
            }
        }

        public Customer Get(string email)
        {
            using(var dbContext = new ECommerceDbContext())
            {
                return dbContext.Customers.SingleOrDefault(c => c.Email == email);
            }
        }
    }

    internal class ECommerceDbContext : DbContext
    {
        static ECommerceDbContext()
        {
            Database.SetInitializer<ECommerceDbContext>(null);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            base.OnModelCreating(modelBuilder);
        }

        public ECommerceDbContext():base("eCommerce")
        {            
        }

        public DbSet<Customer> Customers {get; set;}
    }
   
}
