﻿using eCommerce.Data.MSSQL;
using NUnit.Framework;

namespace eCommerce.Tests
{
    [TestFixture(Category ="Integration")]
    public class CustomerTests
    {
        [Test]
        public void Get_Customer_By_Email_Returns_Customer()
        {
            var email = "spencer.hopkinson@adp.com";

            var customers = new CustomerRepository();        
            
            var customer = customers.Get(email);

            Assert.IsNotNull(customer);
        }
    }
}
